from unittest.mock import MagicMock, call

from eventence import projection as proj
from eventence.ports import EventDataPort as EventData, EventDataTransfer
from eventence._builders import Given, EventDtoBuilder


class StubProjection(proj.Projection):
    def __init__(self):
        self.event_dtos = []

    @property
    def event_hooks(self) -> list[proj.EventHook]:
        return [proj.EventHook("EntityName", "EventName", self.save_event_dto)]

    def save_event_dto(self, value):
        self.event_dtos.append(value)

    @property
    def last_event_id(self):
        return 20


class FeatureProjectorManageProjections:
    def ucase_projector_syncronize_projection_with_event_dtos(self):
        projection = StubProjection()
        event_data = MagicMock(spec=EventData)
        event_dto = (
            EventDtoBuilder()
            .by_entity(None, "module:EntityName")
            .named("EventName")
            .build()
        )
        event_data.collect_event_dtos.return_value = [event_dto]

        projector = proj.Projector(event_data)
        projector.register_projection(projection)

        assert projection.event_dtos == [event_dto]
        event_data.collect_event_dtos.assert_called_with(
            projection.last_event_id, ["EntityName.EventName"]
        )

    def ucase_projector_syncronize_with_event_broker(self):
        ...

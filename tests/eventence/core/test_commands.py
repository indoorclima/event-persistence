import uuid
from unittest.mock import Mock

from eventence.core.commands import Store, EventHandler, EventRegister, EntityNotFound
from eventence._builders import (
    Given,
    Entity,
    given_a_list_of_entity_event_dtos,
    EntityId,
)

from eventence.ports import EventDataPort


class FeatureEventHandlingInsideEntity:
    def teardown_method(self, method):
        EventRegister.unregister_all()

    def setup_method(self, method):
        EventRegister.register_class(Entity)

    def ucase_event_handled_injected_into_entity(self):
        entity = Entity()
        assert type(entity.event_handler) is EventHandler

        EventRegister.unregister_class(Entity)
        entity = Entity()
        assert not hasattr(entity, "event_handler")

    def ucase_events_are_recorded_into_history(self):
        entity = Entity()
        handler = entity.event_handler
        assert handler.version is None
        entity.create(uuid.uuid4(), 1)

        event_dto = handler.history[0]
        assert handler.version == 0
        assert event_dto.version == 0
        assert event_dto.event_name == "Created"

        entity.modify(3, "other")
        event_dto = handler.history[1]
        assert handler.version == 1
        assert event_dto.version == 1
        assert event_dto.event_name == "Modified"
        assert len(handler.history) == 2

        entity.event_handler.clean_history()
        assert handler.version == 1
        assert len(handler.history) == 0

    def ucase_entity_is_build_from_event_dtos(self):
        event_dtos = given_a_list_of_entity_event_dtos(4)

        entity = Entity()
        handler = entity.event_handler
        handler.build_entity(event_dtos)

        assert handler.version == 3
        assert not handler.history
        assert entity.int_attr == event_dtos[-1].event_pars["int_attr"]


class FeatureEventHandlerIsObservable:
    def _ucase_event_handler_notify_event_is_triggered(self):
        raise NotImplementedError()

    def _ucase_event_handler_notify_history_if_exist(self):
        raise NotImplementedError()


class FeatureEntityPersistence:
    def setup_method(self, method):
        EventRegister.register_class(Entity)

    def _given_a_store(self):
        self.event_data = Mock(spec=EventDataPort)
        return Store(self.event_data)

    def _given_a_entity(self, modifications=4):
        event_dtos = given_a_list_of_entity_event_dtos(modifications + 1)

        entity = Entity()
        pars = event_dtos[0].event_pars
        # pars['uuid'] = uuid.UUID(pars['uuid'][4:])
        entity.create(**pars)
        for dto in event_dtos[1:]:
            pars = dto.event_pars
            entity.modify(**pars)

        return entity

    def ucase_entity_is_saved(self):
        entity = self._given_a_entity()
        store = self._given_a_store()
        event_dtos = entity.event_handler.history

        store.save(entity)

        self.event_data.add_event_dtos.assert_called_with(event_dtos)

    def ucase_many_entities_saved_within_same_transaction(self):
        entity_1 = self._given_a_entity()
        entity_2 = self._given_a_entity()
        store = self._given_a_store()
        event_dtos = entity_1.event_handler.history
        event_dtos.extend(entity_2.event_handler.history)

        store.save(entity_1, entity_2)

        self.event_data.add_event_dtos.assert_called_with(event_dtos)

    def ucase_entity_is_loaded_by_its_id(self):
        store = self._given_a_store()
        dtos = given_a_list_of_entity_event_dtos()
        self.event_data.get_entity_event_dtos.return_value = dtos
        uuid_ = dtos[0].event_pars["uuid"]

        entity = store.load(EntityId(uuid_))

        assert entity.id.uuid == uuid_
        assert entity.int_attr == dtos[-1].event_pars["int_attr"]
        assert not entity.event_handler.history
        assert entity.event_handler.version == 4

    def _ucase_entity_with_event_handler_can_be_loaded(self):
        store = self._given_a_store()

        raise NotImplementedError()

    def ucase_return_none_if_no_exist_entity(self):
        store = self._given_a_store()
        uuid_ = uuid.uuid1()
        self.event_data.get_entity_event_dtos.side_effect = EntityNotFound(
            EntityId(uuid_)
        )

        assert store.load(EntityId(uuid_)) is None

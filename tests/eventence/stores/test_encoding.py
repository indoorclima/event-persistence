from eventence._builders import Given
import eventence.stores.encoding as encoding
import datetime
import pytest
import uuid


class FeatureDatetimeIsoDecoderDecodesStringToDatetime:
    def ucase_decode_datetime_from_isoformat_string(self):
        now = datetime.datetime.now()
        decoder = encoding.DatetimeStringDecoder
        assert now == decoder.decode(now.isoformat())

    def ucase_isoformat_is_validated(self):
        decoder = encoding.DatetimeStringDecoder

        value = datetime.datetime.now().isoformat()
        assert decoder.is_isoformat(value)

        value = value.replace("T", "a")
        assert not decoder.is_isoformat(value)
        assert not decoder.is_isoformat(1)
        assert not decoder.is_isoformat("10101010")

        assert decoder.is_isoformat("2010-10-22")
        assert decoder.is_isoformat("2020-11-18 00:05:23.283+00:00")


class FeatureValueDictEncoderConvertValuesToDict:
    def ucase_encoding_plain_value_objects(self):
        encoder = encoding.ValueEncoder
        value = Given().a_value.with_invariants(par_int=1, part_str="str").build()
        Value = value.__class__

        dict_value = value.invariants
        dict_value["__value__"] = "{}:{}".format(Value.__module__, Value.__name__)
        assert dict_value == encoder.encode(value)

    def ucase_encoding_nested_values(self):
        encoder = encoding.ValueEncoder
        given = Given()
        nested_value = given.a_value.with_invariants(par_int=1).build()
        value = given.a_value.with_invariants(par_str="str", value=nested_value).build()

        encoded = encoder.encode(value)
        assert encoded["value"] == encoder.encode(nested_value)

    def ucase_encoding_id_values(self):
        encoder = encoding.ValueEncoder
        entity_id = Given().a_entity_id.build()

        expected = {
            "__value__": "eventence._builders:EntityId",
            "uuid": f"HEX:{entity_id.uuid.hex}",
        }

        encoded = encoder.encode(entity_id)
        assert encoded == expected

    def ucase_value_is_validated(self):
        value = Given().a_value.build()
        encoder = encoding.ValueEncoder

        assert encoder.is_value(value)

        value = object()
        assert not encoder.is_value(value)


class FeatureValueDictDecoderConvertsDictToValues:
    def ucase_decoding_dict_to_values(self):
        decoder = encoding.ValueDictDecoder
        value = Given().a_value.with_invariants(par_int=1, par_str="str").build()
        value_dict = {
            "__value__": "eventence._builders:Value",
            "par_int": 1,
            "par_str": "str",
        }

        assert value == decoder.decode(value_dict)

    def ucase_decoding_nested_values(self):
        decoder = encoding.ValueDictDecoder()
        given = Given()
        nested_value = given.a_value.with_invariants(par_int=2).build()
        value = given.a_value.with_invariants(par_int=1, par_value=nested_value).build()

        topic = "eventence._builders:Value"
        value_dict = {
            "__value__": topic,
            "par_int": 1,
            "par_value": {"__value__": topic, "par_int": 2},
        }

        assert value == decoder.decode(value_dict)

    def ucase_decoding_absent_values_raise_ex(self):
        decoder = encoding.ValueDictDecoder()

        topic = "no.exist.this.route:FakeValue"
        value_dict = {
            "__value__": topic,
            "par_int": 1,
            "par_value": {"__value__": topic, "par_int": 2},
        }
        try:
            decoder.decode(value_dict)
            pytest.fail("ValueClassNotFound should be raised")
        except encoding.ValueClassNotFound:
            pass


class FeatureInvariantsEncoding:
    def ucase_encoding_basic_values(self):
        encoder = encoding.InvariantsEncoder
        invariants = {"int_attr": 1, "str_attr": "str", "float_attr": 1.2}

        assert invariants == encoder.encode(invariants)

    def ucase_encoding_datetime(self):
        encoder = encoding.InvariantsEncoder
        now = datetime.datetime.now()

        invariants = {"now": now}

        assert {"now": now.isoformat()} == encoder.encode(invariants)

    def ucase_encoding_uuid_values(self):
        encoder = encoding.InvariantsEncoder
        a_uuid = uuid.uuid4()

        invariants = {"uuid": a_uuid}

        assert {"uuid": f"HEX:{a_uuid.hex}"} == encoder.encode(invariants)

    def ucase_encoding_value_objects(self):
        encoder = encoding.InvariantsEncoder
        a_value = Given().a_value.with_invariants(int_attr=1).build()

        invariants = {"value": a_value}
        expected = {"value": {"__value__": "eventence._builders:Value", "int_attr": 1}}

        assert expected == encoder.encode(invariants)

    def ucase_encoding_nested_value_objects(self):
        encoder = encoding.InvariantsEncoder
        a_value = Given().a_value.with_invariants(int_attr=1).build()
        nested_value = Given().a_value.with_invariants(value=a_value).build()

        invariants = {"value": nested_value}
        expected = {
            "value": {
                "__value__": "eventence._builders:Value",
                "value": {"__value__": "eventence._builders:Value", "int_attr": 1},
            }
        }

        assert expected == encoder.encode(invariants)

    def ucase_no_encoding_dicts(self):
        encoder = encoding.InvariantsEncoder
        try:
            encoder.encode({"key": {"a_dict": 2}})
        except TypeError as e:
            assert str(e) == "Value {'a_dict': 2} is dict type, not a invariant"

    def ucase_no_encoding_lists(self):
        encoder = encoding.InvariantsEncoder
        try:
            encoder.encode({"key": [1, 2]})
        except TypeError as e:
            assert str(e) == 'Field "key" is list type, not a invariant'

    def ucase_encoding_none_values(self):
        encoder = encoding.InvariantsEncoder

        expected = {"key": None}

        encoded = encoder.encode(expected)
        assert expected == encoded

    def ucase_encoding_list_of_invariants(self):
        encoder = encoding.InvariantsEncoder

        encoded = encoder.encode({"key": [1, "hola"]})

        assert encoded == {"key": (1, "hola")}


class Fake:
    def __repr__(self):
        return "Fake"


class FeatureInvariantsDecoding:
    def ucase_decoding_basic_values(self):
        decoder = encoding.InvariantsDecoder
        invariants = {"int_attr": 1, "str_attr": "str", "float_attr": 1.2}

        assert invariants == decoder.decode(invariants)

    def ucase_decoding_no_invariants_raise_error(self):
        decoder = encoding.InvariantsDecoder
        invariants = {"no valid": {"int": Fake()}}

        try:
            decoder.decode(invariants)
            pytest.fail("Type error should be raised")
        except TypeError as e:
            print(str(e))
            assert str(e) == "Value Fake is a Fake type, not a invariant"

    def ucase_decoding_datetime(self):
        decoder = encoding.InvariantsDecoder
        now = datetime.datetime.now()
        invariants = {"a date": now.isoformat()}

        assert {"a date": now} == decoder.decode(invariants)

    def ucase_decoding_uuid_values(self):
        decoder = encoding.InvariantsDecoder
        a_uuid = uuid.uuid4()
        invariants = {"a uuid": f"HEX:{a_uuid.hex}"}

        assert {"a uuid": a_uuid} == decoder.decode(invariants)

    def ucase_decoding_value_objects(self):
        decoder = encoding.InvariantsDecoder
        invariants = {"value": {"__value__": "eventence._builders:Value", "par_int": 1}}

        value = Given().a_value.with_invariants(par_int=1).build()
        assert {"value": value} == decoder.decode(invariants)

    def ucase_decoding_nested_value_objects(self):
        invariants = {
            "value": {
                "__value__": "eventence._builders:Value",
                "value": {"__value__": "eventence._builders:Value", "int_attr": 1},
            }
        }

        given = Given()
        nested_value = given.a_value.with_invariants(int_attr=1).build()
        value = given.a_value.with_invariants(value=nested_value).build()

        decoder = encoding.InvariantsDecoder

        assert {"value": value} == decoder.decode(invariants)

    def ucase_decoding_list_or_tuples(self):
        invariants = {"value": [1, "hola"]}
        decoder = encoding.InvariantsDecoder

        assert {"value": (1, "hola")} == decoder.decode(invariants)

    def ucase_decoding_none_value(self):
        invariant = {"key": None}

        decoder = encoding.InvariantsDecoder

        assert invariant == decoder.decode(invariant)

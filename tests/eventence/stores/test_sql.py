from unittest.mock import Mock, call
from eventence.stores.sql import (
    SqlEventDataAdapter,
    SqlCommand,
    SqlProjectionCommander,
    EventAlreadyProcessed,
    EventTable,
    EntityTable,
    EventView,
)
from eventence._builders import given_a_list_of_entity_event_dtos

import pytest


class FeatureEventDataAdapter:
    def _given_a_sql_event_data_adapter(self):
        self.conn = Mock(__enter__=Mock(), __exit__=Mock())
        self.event_table = Mock(spec=EventTable)
        self.entity_table = Mock(spec=EntityTable)
        self.event_view = Mock(spec=EventView)
        return SqlEventDataAdapter(
            self.conn, self.event_table, self.entity_table, self.event_view
        )

    def ucase_many_event_dtos_are_inserted_into_data(self):
        data = self._given_a_sql_event_data_adapter()
        event_dtos = given_a_list_of_entity_event_dtos()
        cursor = self.conn.cursor.return_value

        data.add_event_dtos(event_dtos)

        expecteds = [call(cursor, event_dto) for event_dto in event_dtos]
        for expected in expecteds:
            assert expected in self.event_table.insert_event.mock_calls

    def ucase_entity_is_inserted_when_version_0_is_found(self):
        data = self._given_a_sql_event_data_adapter()
        event_dtos = given_a_list_of_entity_event_dtos()
        cursor = self.conn.cursor.return_value

        data.add_event_dtos(event_dtos)
        self.entity_table.insert_entity.assert_called_with(cursor, event_dtos[0])

    def ucase_get_event_dtos_from_an_entity(self):
        data = self._given_a_sql_event_data_adapter()
        cursor = self.conn.cursor.return_value

        hex_value = "hex-value"
        event_dtos = data.get_entity_event_dtos(hex_value)

        self.event_view.select_event_dtos_by_entity_uuid.assert_called_with(
            cursor, "hex-value"
        )

        assert (
            event_dtos == self.event_view.select_event_dtos_by_entity_uuid.return_value
        )

    def ucase_get_all_event_dtos_from_event_type_filter(self):
        data = self._given_a_sql_event_data_adapter()
        data.get_where_event_types = Mock(return_value="event-filters")
        cursor = self.conn.cursor.return_value

        event_dtos = data.collect_event_dtos(4, ["Part.Created", "Entity.Modified"])

        self.event_view.select_all_event_dtos.assert_called_with(
            cursor, 4, "event-filters"
        )

        assert event_dtos == self.event_view.select_all_event_dtos.return_value

    def ucase_filter_event_works(self):
        data = self._given_a_sql_event_data_adapter()

        value = data.get_where_event_types(
            ["Part.Created", "Requirement.Defined", "Part.OtherEvent"]
        )

        assert (
            value
            == " and ((entity_name='Part' and (name='Created' or name='OtherEvent')) or (entity_name='Requirement' and (name='Defined')))"
        )

    def ucase_schema_is_autocreated(self):
        event_table = Mock()
        event_table.exist.return_value = False
        event_view = Mock()
        event_view.exist.return_value = False
        entity_table = Mock()
        entity_table.exist.return_value = False
        conn = Mock(__enter__=Mock(), __exit__=Mock())
        cursor = conn.cursor()

        SqlEventDataAdapter(conn, event_table, entity_table, event_view)

        event_table.create.assert_called_with(cursor)
        event_view.create.assert_called_with(cursor)
        entity_table.create.assert_called_with(cursor)
        event_table.exist.assert_called_with(cursor)
        event_view.exist.assert_called_with(cursor)
        entity_table.exist.assert_called_with(cursor)

    def ucase_schema_is_not_created_if_already_exist(self):
        event_table = Mock()
        event_table.exist.return_value = True
        event_view = Mock()
        event_view.exist.return_value = True
        entity_table = Mock()
        entity_table.exist.return_value = True
        conn = Mock(__enter__=Mock(), __exit__=Mock())

        SqlEventDataAdapter(conn, event_table, entity_table, event_view)

        event_table.create.assert_not_called()
        event_view.create.assert_not_called()
        entity_table.create.assert_not_called()


class FeatureSqlCommandData:
    def _given_a_sql_command(self):
        return SqlCommand(
            "select * from Table where field1 = ? and field2 = ? " "and field3 = 3",
            (1, 2, 3),
        )

    def ucase_sqlcommand_is_inmutable(self):
        command = self._given_a_sql_command()
        try:
            command.statement = "select * from Table"
            pytest.fail("Attribute error should be raised when attribute modidified")

        except AttributeError:
            pass

        try:
            command.new_field = "select"
            pytest.fail("Attribute error should be raised when new attribute")
        except AttributeError:
            pass

        try:
            del command.parameters
            pytest.fail("Attribute error should be raised when attribute deleted")
        except AttributeError:
            pass

    def ucase_sql_command_validate_data(self):
        try:
            SqlCommand(1, (1, 2))
            pytest.fail("Type error should be raised if statement is not int")
        except TypeError as e:
            assert str(e) == "Statement argument shall be type string, not int"

        try:
            SqlCommand("  ", 1)
            pytest.fail("Type error should be raised if statement is not int")
        except TypeError as e:
            assert (
                str(e)
                == "Parameters argument type could be list, dict, tuple or None, but not int"
            )


class FeatureSqlProjectionCommandHandling:
    def _given_a_sql_projection_commander(self):
        self.connection = connection = Mock(__enter__=Mock(), __exit__=Mock())
        self.cursor = self.connection.cursor.return_value
        self.projection_name = projection_name = "proj"
        self.event_index_update_statement = "event-index-update-statement"
        self.event_index_select_statement = "event-index-select-statement"
        self.schema_statements = ["create table", "create index"]
        return SqlProjectionCommander(
            connection,
            projection_name,
            self.event_index_update_statement,
            self.event_index_select_statement,
            self.schema_statements,
        )

    def _when_event_index_is(self, event_id):
        self.cursor.query_one.return_value = (event_id,)

    def ucase_commander_create_schema_if_exception_getting_event_index(self):
        conn = Mock(__enter__=Mock(), __exit__=Mock())
        cursor = conn.cursor.return_value
        cursor.fetchone.side_effect = [Exception(), (1,)]
        commander = SqlProjectionCommander(
            conn,
            "proj",
            "event-index-update-statement",
            "event-index-update-statement",
            ["create table", "create index"],
        )

        assert call("create table") in cursor.execute.mock_calls
        assert call("create index") in cursor.execute.mock_calls

        assert commander.get_event_index() == 1

    def ucase_commander_update_event_id_for_each_command(self):
        commander = self._given_a_sql_projection_commander()
        command = SqlCommand("insert data", ("par1",))
        event_id = 1
        self.cursor.fetchone.return_value = (0,)

        commander.update_data_with_event(command, event_id)

        expecteds = [
            call(command.statement, command.parameters),
            call(self.event_index_select_statement),
            call(self.event_index_update_statement, (1,)),
        ]
        for expect in expecteds:
            assert expect in self.cursor.execute.mock_calls

    def ucase_commander_detect_old_event_commands(self):
        commander = self._given_a_sql_projection_commander()
        command = SqlCommand("insert data", ("par1",))
        self.cursor.fetchone.return_value = (3,)

        try:
            commander.update_data_with_event(command, 2)
            pytest.fail("EventAlreadyProcessed should be raised")
        except EventAlreadyProcessed as e:
            assert (
                str(e) == 'Event with id 2 was already processed by projection "proj"'
            )

    def ucase_commander_return_queries(self):
        commander = self._given_a_sql_projection_commander()
        command = SqlCommand("select command", (1,))
        self.cursor.fetchone.return_value = (3, 0)
        self.cursor.fetchall.return_value = [(1, 2), (3, 4)]

        assert commander.query_value(command) == 3
        self.cursor.execute.assert_called_with("select command", (1,))

        assert commander.query_one(command) == (3, 0)

        assert commander.query_many(command) == [(1, 2), (3, 4)]

from eventence.stores.sqlite import SqliteEventTable, SqliteEntityTable, SqliteEventView

from eventence._builders import given_a_list_of_entity_event_dtos
from eventence.core.commands import (
    EntityAlreadyExist,
    EntityNotFound,
    EntityVersionAlreadyExist,
)
import sqlite3
import uuid
import pytest


class FeatureEntityWriting:
    def _given_a_created_entity_table(self):
        self.conn = sqlite3.connect(":memory:")
        with self.conn:
            cursor = self.conn.cursor()
            table = SqliteEntityTable()
            table.create(cursor)
        return table

    def ucase_creating_schema(self):
        conn = sqlite3.connect(":memory:")
        with conn:
            cursor = conn.cursor()
            entity_table = SqliteEntityTable()
            assert not entity_table.exist(cursor)

            entity_table.create(cursor)

        assert entity_table.exist(conn.cursor())

    def ucase_inserting_entity(self):
        event_dto = given_a_list_of_entity_event_dtos(1)[0]
        table = self._given_a_created_entity_table()

        with self.conn:
            cursor = self.conn.cursor()
            table.insert_entity(cursor, event_dto)

        cursor = self.conn.cursor()
        assert table.select_entity(cursor, event_dto.entity_uuid) == {
            "entity_uuid": event_dto.entity_uuid,
            "entity_name": event_dto.entity_name,
        }

    def ucase_inserting_duplicated_entity_raise_exception(self):
        event_dto = given_a_list_of_entity_event_dtos(1)[0]
        table = self._given_a_created_entity_table()

        with self.conn:
            cursor = self.conn.cursor()
            table.insert_entity(cursor, event_dto)

        try:
            cursor = self.conn.cursor()
            table.insert_entity(cursor, event_dto)
            pytest.fail("EntityAlreadyExist should be raised")
        except EntityAlreadyExist as e:
            assert (
                str(e) == f"Entity with uuid {event_dto.entity_uuid}"
                " already persisted."
            )


class FeatureEventWriting:
    def _given_a_created_event_table(self):
        self.conn = sqlite3.connect(":memory:")
        with self.conn:
            cursor = self.conn.cursor()
            # self.entity_table = SqliteEntityTable()
            # self.entity_table.create(cursor)
            table = SqliteEventTable()
            table.create(cursor)
        return table

    def ucase_creating_schema(self):
        conn = sqlite3.connect(":memory:")
        with conn:
            cursor = conn.cursor()
            SqliteEntityTable().create(cursor)
            event_table = SqliteEventTable()
            assert not event_table.exist(cursor)

            event_table.create(cursor)

        assert event_table.exist(conn.cursor())

    # def ucase_entity_table_shall_be_created_befoer(self):
    #     conn = sqlite3.connect(':memory:')
    #     with conn:
    #         cursor = conn.cursor()
    #         event_table = SqliteEventTable()
    #         assert not event_table.exist(cursor)

    #         event_table.create(cursor)

    #     assert event_table.exist(conn.cursor())

    def ucase_inserting_event(self):
        event_dto = given_a_list_of_entity_event_dtos(1)[0]
        table = self._given_a_created_event_table()

        cursor = self.conn.cursor()
        event_id = table.insert_event(cursor, event_dto)

        event_dto.id = event_id
        expected = {
            "id": event_id,
            "created_on": event_dto.created_on,
            "entity_uuid": event_dto.entity_uuid,
            "event_name": event_dto.event_name,
            "version": event_dto.version,
            "event_pars": event_dto.event_pars,
        }

        assert table.select_event(cursor, event_id) == expected
        # assert (self.entity_table.select_entity(cursor, event_dto['entity_id']))

    def ucase_inserting_duplicated_version_raise_exception(self):
        event_dto = given_a_list_of_entity_event_dtos(2)[1]
        table = self._given_a_created_event_table()

        with self.conn:
            cursor = self.conn.cursor()
            table.insert_event(cursor, event_dto)

        try:
            cursor = self.conn.cursor()
            table.insert_event(cursor, event_dto)
            pytest.fail("EntityAlreadyExist should be raised")
        except EntityVersionAlreadyExist:
            pass


class FeatureEventDtoReading:
    def _given_some_event_dtos_stored(self, conn):
        with conn:
            cursor = conn.cursor()
            entity_table = SqliteEntityTable()
            event_table = SqliteEventTable()

        events_by_entity = []
        for _ in range(3):
            events_by_entity.append(given_a_list_of_entity_event_dtos(5))

        all_event_dtos = []
        for i in range(5):
            all_event_dtos.extend(
                [events_by_entity[0][i], events_by_entity[1][i], events_by_entity[2][i]]
            )

        with conn:
            cursor = conn.cursor()
            entity_table.insert_entity(cursor, all_event_dtos[0])
            entity_table.insert_entity(cursor, all_event_dtos[1])
            entity_table.insert_entity(cursor, all_event_dtos[2])
            for dto in all_event_dtos:
                event_table.insert_event(cursor, dto)
        return all_event_dtos

    def _given_a_entity_events_view(self):
        conn = sqlite3.connect(":memory:")
        view = SqliteEventView()
        with conn:
            cursor = conn.cursor()
            SqliteEventTable().create(cursor)
            SqliteEntityTable().create(cursor)
            view.create(cursor)
        return conn, view

    def ucase_creating_schema(self):
        conn = sqlite3.connect(":memory:")
        with conn:
            cursor = conn.cursor()
            view = SqliteEventView()
            assert not view.exist(cursor)
            view.create(cursor)

        assert view.exist(conn.cursor())

    def ucase_if_no_events_no_entity_found(self):
        conn, events_view = self._given_a_entity_events_view()
        cursor = conn.cursor()
        entity_uuid = uuid.uuid4()
        try:
            events_view.select_event_dtos_by_entity_uuid(cursor, entity_uuid)
            pytest.fail("EntityNotFound should be raised")
        except EntityNotFound:
            pass

    def ucase_get_event_dtos_from_an_entity(self):
        conn, events_view = self._given_a_entity_events_view()
        event_dtos = self._given_some_event_dtos_stored(conn)
        for id, dto in enumerate(event_dtos):
            dto.id = id + 1

        cursor = conn.cursor()
        entity_uuid = event_dtos[0].entity_uuid
        dtos = events_view.select_event_dtos_by_entity_uuid(cursor, entity_uuid)

        print(dtos)
        assert dtos == event_dtos[0:-1:3]

    def ucase_get_event_dtos_since_id_and_filtered(self):
        conn, events_view = self._given_a_entity_events_view()
        event_dtos = self._given_some_event_dtos_stored(conn)
        for id, dto in enumerate(event_dtos):
            dto.id = id + 1

        cursor = conn.cursor()
        dtos = events_view.select_all_event_dtos(cursor, 5)

        assert dtos == event_dtos[5:]

        dtos = events_view.select_all_event_dtos(cursor, 0, " and (name = 'Created')")
        assert len(dtos) == 3

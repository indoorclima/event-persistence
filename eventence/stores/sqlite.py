"""Persistence infrastructure for event sources using Sqlite databases.

As a developer, I should easily setup a persistence layer for event sourcing
using a popular database, like Sqlite is.
"""

import sqlite3
import uuid
import json
import datetime

from eventence.stores.sql import EventTable, EntityTable, EventView, SqlEventDataAdapter
from eventence.core.commands import (
    EntityAlreadyExist,
    EntityNotFound,
    EntityVersionAlreadyExist,
)
from eventence.ports import EventDataTransfer
from eventence.stores.encoding import InvariantsDecoder, InvariantsEncoder


class SqliteEventDataAdapter(SqlEventDataAdapter):
    """Implementation of EventDataPort using sqlite database."""

    def __init__(self, filename, **kwargs):
        """Implement EventData using sqlite database."""
        self.conn = sqlite3.connect(filename, check_same_thread=False, **kwargs)
        super().__init__(
            self.conn, SqliteEventTable(), SqliteEntityTable(), SqliteEventView()
        )


class SqliteEventTable(EventTable):
    """Persist event details in a table."""

    def exist(self, cursor):
        """Exist table?."""
        cursor.execute(
            "SELECT name FROM sqlite_master WHERE type='table'" " AND name='events';"
        )
        return cursor.fetchone() is not None

    def create(self, cursor):
        """Create entities table into SQlite database."""
        statements = [
            """
            CREATE TABLE "events" (
            "id" INTEGER,
            "created_on" timestamp,
            "entity_id" TEXT,
            "name" TEXT,
            "version" INTEGER,
            "pars" TEXT,
            PRIMARY KEY("id"),
            foreign key(entity_id) references entities(id)); """,
            """
            create unique index "events_select" on "events" (
            "entity_id",
            "version");""",
        ]
        for statement in statements:
            cursor.execute(statement)

    def insert_event(self, cursor, event_dto):
        """Insert into events table event information."""
        try:
            version = event_dto.version
            entity_hex = event_dto.entity_uuid.hex
            cursor.execute(
                """insert into events (created_on, entity_id, name, version, pars)
                values (?, ?, ?, ?, ?) """,
                (
                    event_dto.created_on,
                    entity_hex,
                    event_dto.event_name,
                    version,
                    json.dumps(InvariantsEncoder.encode(event_dto.event_pars)),
                ),
            )
        except sqlite3.IntegrityError:
            raise EntityVersionAlreadyExist(event_dto.entity_uuid, event_dto.version)

        return cursor.lastrowid

    def select_event(self, cursor, event_id):
        """Select events by event_id."""
        cursor.execute("select * from events where id=?", (event_id,))
        row = cursor.fetchone()
        if row:
            return {
                "id": row[0],
                "created_on": datetime.datetime.fromisoformat(row[1]),
                "entity_uuid": uuid.UUID(row[2]),
                "event_name": row[3],
                "version": row[4],
                "event_pars": InvariantsDecoder.decode(json.loads(row[5])),
            }


class SqliteEntityTable(EntityTable):
    """Entity is written on sql table."""

    def exist(self, cursor):
        """Exist table in database?."""
        cursor.execute(
            "SELECT name FROM sqlite_master WHERE type='table'" " AND name='entities';"
        )
        row = cursor.fetchone()

        cursor.execute("pragma foreign_keys=ON;")
        return row is not None

    def create(self, cursor):
        """Create entities table into SQlite database."""
        statements = [
            """CREATE TABLE "entities" (
            "id" TEXT,
            "module" TEXT,
            "name" TEXT,
            primary key("id"))""",
            """ CREATE INDEX "entities_name" ON "entities" (
            "module", "name") """,
        ]
        for statement in statements:
            cursor.execute(statement)

    def insert_entity(self, cursor, event_dto):
        """Insert entity into database."""
        module, name = event_dto.entity_name.split(":")
        entity_hex = event_dto.entity_uuid.hex
        try:
            cursor.execute(
                """
            insert into entities (id, module, name)
            values (?, ?, ?)""",
                (entity_hex, module, name),
            )
        except sqlite3.IntegrityError:
            raise EntityAlreadyExist(uuid.UUID(entity_hex))

    def select_entity(self, cursor, entity_uuid):
        """Return entity_dto from entity_id."""
        cursor.execute("select * from entities where id= ?", (entity_uuid.hex,))
        row = cursor.fetchone()

        if row:
            return {
                "entity_uuid": uuid.UUID(row[0]),
                "entity_name": f"{row[1]}:{row[2]}",
            }


class SqliteEventView(EventView):
    """Read event_dtos from sqlite database."""

    def exist(self, cursor):
        """Exist the view?."""
        cursor.execute(
            "SELECT name FROM sqlite_master WHERE type='view'"
            " AND name='entity_events';"
        )
        return cursor.fetchone() is not None

    def create(self, cursor):
        """Create sql view."""
        cursor.execute(
            """create view entity_events
            as select
            events.id as id, events.name as name, version, created_on,
            entities.id as entity_id, entities.name as entity_name,
            entities.module as entity_module, pars
            from
            events inner join entities on events.entity_id = entities.id"""
        )

    def select_event_dtos_by_entity_uuid(self, cursor, entity_uuid):
        """Select all event_dtos of an entity."""
        cursor.execute(
            """select * from entity_events where entity_id = ?
                       order by id""",
            (entity_uuid.hex,),
        )
        rows = cursor.fetchall()
        if not rows:
            raise EntityNotFound(entity_uuid)
        return self._decode_rows(rows)

    def select_all_event_dtos(self, cursor, since_id=0, where_event_filters=""):
        """Select all event_dtos since event id with event type filters."""
        statement = (
            "select * from entity_events where id > ? "
            f"{where_event_filters} order by id"
        )
        cursor.execute(statement, (since_id,))
        rows = cursor.fetchall()
        if not rows:
            return
        return self._decode_rows(rows)

    def _decode_rows(self, rows):
        event_dtos = []
        for row in rows:
            event_dtos.append(
                EventDataTransfer(
                    id=row[0],
                    event_name=row[1],
                    version=row[2],
                    created_on=datetime.datetime.fromisoformat(row[3]),
                    entity_uuid=uuid.UUID(row[4]),
                    entity_name=f"{row[6]}:{row[5]}",
                    event_pars=InvariantsDecoder.decode(json.loads(row[7])),
                )
            )

        return event_dtos

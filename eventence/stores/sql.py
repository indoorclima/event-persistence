"""
Implement persistence with SQL database.

As a developer, I would like to easily create a persistence environment
with SQL database so I only have to define tables and views.
"""
import abc
import threading

from eventence.ports import EventDataPort


class SqlEventDataAdapter(EventDataPort):
    """Thread safe event data adapter for SQL databases."""

    def __init__(self, connection, event_table, entity_table, event_view):
        """Adapt EventDataPort to SQL databases."""
        self._conn = connection
        self._lock = threading.Lock()

        # We only need 2 tables and 1 view!!!
        self._event_table = event_table
        self._entity_table = entity_table
        self._event_view = event_view

        self._create_schema_if_doesnt_exist()

    def _create_schema_if_doesnt_exist(self):
        tables = [self._entity_table, self._event_table, self._event_view]
        with self._conn:
            cursor = self._conn.cursor()
            for table in tables:
                if not table.exist(cursor):
                    table.create(cursor)

    def get_entity_event_dtos(self, entity_uuid):
        """Return all event dtos persisted on entity."""
        with self._lock:
            cursor = self._conn.cursor()
            return self._event_view.select_event_dtos_by_entity_uuid(
                cursor, entity_uuid
            )

    def add_event_dtos(self, event_dtos):
        """Save all event dtos within a unique transaction."""
        with self._lock:
            with self._conn:
                cursor = self._conn.cursor()
                for event_dto in event_dtos:
                    if event_dto.version == 0:
                        self._entity_table.insert_entity(cursor, event_dto)

                    event_dto.id = self._event_table.insert_event(cursor, event_dto)

    def collect_event_dtos(self, last_event_id=0, events_filter=None):
        """Return event_dtos by event type filter since entity id."""
        with self._lock:
            cursor = self._conn.cursor()

            return self._event_view.select_all_event_dtos(
                cursor, last_event_id, self.get_where_event_types(events_filter)
            )

    def get_where_event_types(self, events_filter):
        """Return where clausure to insert into collect event statement."""
        grouping = {}
        for filter_ in events_filter:
            class_name, event_name = filter_.split(".")
            if class_name not in grouping:
                grouping[class_name] = []

            grouping[class_name].append(event_name)
        sqls = []
        for class_name, events in grouping.items():
            class_sql = f"entity_name='{class_name}'"
            events_sql = [f"name='{event}'" for event in events]
            events_sql = " or ".join(events_sql)
            sqls.append(f"({class_sql} and ({events_sql}))")

        return " and ({})".format(" or ".join(sqls))


class EventTable(abc.ABC):
    """Interface of sql event table."""

    @abc.abstractmethod
    def exist(self, cursor):
        """Confirm if table exist in database."""
        pass

    @abc.abstractmethod
    def create(self, cursor):
        """Create table into database."""
        pass

    @abc.abstractmethod
    def insert_event(self, cursor, event_dto):
        """Insert event_dto into table."""
        pass


class EntityTable(abc.ABC):
    @abc.abstractmethod
    def exist(self, cursor):
        pass

    @abc.abstractmethod
    def create(self, cursor):
        pass

    @abc.abstractmethod
    def insert_entity(self, cursor, event_dto):
        pass


class EventView(abc.ABC):
    @abc.abstractmethod
    def exist(self, cursor):
        pass

    @abc.abstractmethod
    def create(self, cursor):
        pass

    @abc.abstractmethod
    def select_event_dtos_by_entity_uuid(self, cursor, entity_uuid):
        pass

    @abc.abstractmethod
    def select_all_event_dtos(self, cursor, from_id=0, where_event_types=""):
        pass


class SqlCommand:
    """Value object for executing sql commands."""

    def __init__(self, statement, parameters=None):
        """Construct data class for a SQL command."""
        self._validate(statement, parameters)
        self.__dict__.update(
            {"statement": statement, "parameters": self._cast_pars(parameters)}
        )

    @staticmethod
    def _cast_pars(parameters):
        if type(parameters) is list:
            return tuple(parameters)

        if type(parameters) is dict or type(parameters) is tuple:
            return parameters

    def _validate(self, statement, parameters):
        if type(statement) is not str:
            raise TypeError(
                f"Statement argument shall be type string, not {type(statement).__name__}"
            )
        if not (
            type(parameters) is list
            or type(parameters) is dict
            or type(parameters) is tuple
            or parameters is None
        ):
            raise TypeError(
                "Parameters argument type could be list, dict, tuple or None, but not "
                f"{type(parameters).__name__}"
            )

    def __setattr__(self, name, value):
        """Implement inmutable feature."""
        raise AttributeError(
            f"Inmutable class, not possible change/add attribute {name}"
        )

    def __delattr__(self, name):
        """Implement inmutable feature."""
        raise AttributeError("Inmutable object, not possible to delete any attribute")


class EventAlreadyProcessed(Exception):
    def __init__(self, event_id, projection_name):
        """Exception when event dto was already processed by projection."""
        self._id = event_id
        super().__init__(
            f"Event with id {event_id} was already "
            f'processed by projection "{projection_name}"'
        )


class SqlProjectionCommander:
    def __init__(
        self,
        connection,
        projection_name,
        event_index_update_statement,
        event_index_select_statement,
        schema_statements,
    ):
        """Command helper for ProjectionData for SQL databases."""
        self._conn = connection
        self._projection_name = projection_name
        self._update_index = event_index_update_statement
        self._select_index = event_index_select_statement
        self._schema_commands = [
            SqlCommand(statement) for statement in schema_statements
        ]
        if not self._has_schema():
            self._create_schema()

    def update_data_with_event(self, command, event_id):
        """Update projection data from event information."""
        if event_id <= self.get_event_index():
            raise EventAlreadyProcessed(event_id, self._projection_name)

        with self._conn:
            cursor = self._conn.cursor()
            self._execute(cursor, command)
            cursor.execute(self._update_index, (event_id,))

    def _execute(self, cursor, command):
        if command.parameters is None:
            cursor.execute(command.statement)
        else:
            cursor.execute(command.statement, command.parameters)

    def _create_schema(self):
        with self._conn:
            cursor = self._conn.cursor()
            for command in self._schema_commands:
                self._execute(cursor, command)
            self._conn.commit()

    def _has_schema(self):
        try:
            self.get_event_index()
            return True
        except Exception:
            return False

    def query_value(self, command):
        """Return a value from a select command."""
        return self.query_one(command)[0]

    def query_one(self, command):
        """Return a row from a select command."""
        cursor = self._conn.cursor()
        self._execute(cursor, command)
        return cursor.fetchone()

    def query_many(self, command):
        """Return a list of rows from a select command."""
        cursor = self._conn.cursor()
        self._execute(cursor, command)
        return cursor.fetchall()

    def get_event_index(self):
        """Return id of last event processed by projection."""
        return self.query_value(SqlCommand(self._select_index))

import psycopg2.extras


psycopg2.extras.register_uuid()


create_event_schema = [
    """CREATE TABLE aggregates (
    "id" uuid,
    "module" text,
    "name" varchar(100),
    constraint aggregates_pkey PRIMARY KEY (id)
    );
    """,
    """
    CREATE TABLE events (
    "id" serial primary key,
    "created_on" timestamp,
    "aggregate_id" uuid,
    "name" varchar(100),
    "version" integer,
    "pars" text,
    "inserted_on" timestamp default now(),
    foreign key (aggregate_id) references aggregates (id));
    """,
    """
    create index aggregate_idx on aggregates (module, name);
    """,
    """
    create unique index "events_select" on events ("aggregate_id", "version");
    """,
    """
    create view aggregate_events
    as
    select
       events.id as id, events.name as name, version, created_on,
       aggregates.id as aggregate_id, aggregates.name as aggregate_name,
       aggregates.module as aggregate_module, pars
    from
       events inner join aggregates on events.aggregate_id = aggregates.id;
    """,
]

select_event_index = "select max(id) from events"


select_aggregate_events = """
    select * from aggregate_events where aggregate_id=%s order by version asc
"""

insert_event = """
insert into events (created_on, aggregate_id, name, version, pars)
values (%(created_on)s, %(aggregate_id)s, %(name)s, %(version)s, %(pars)s)
returning id
"""


insert_aggregate = """
insert into aggregates (id, module, name)
values (%(aggregate_id)s, %(aggregate_module)s, %(aggregate_name)s)"""


select_all_entity_events = """
select *
from aggregate_events
where id > %s and ({events_filter}) order by id"""

"""Helper module for encoding and decoding domain data."""
import uuid
import re
import datetime
import importlib
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class DatetimeStringDecoder:
    """Namespace for decoding isoformat string to datetime object."""

    _iso_format = re.compile(
        r"^(?P<full>((?P<year>\d{4})([/-](?P<mon>(0[1-9])|(1[012]))([/-](?P<md"
        r"ay>(0[1-9])|([12]\d)|(3[01]))))(?:[T ](?P<hour>([01][0-9])|(?:2[01"
        r"23]))(\:?(?P<min>[0-5][0-9])(\:?(?P<sec>[0-5][0-9]([\,\.]\d{1,10})?)"
        r")?)?(?:Z|([\-+](?:([01][0-9])|(?:2[0123]))(\:?(?:[0-5][0-9]))?))?)?)"
        r")$"
    )

    @classmethod
    def is_isoformat(cls, value):
        """Confirm if value is a datetime isoformat representation."""
        return type(value) is str and bool(re.match(cls._iso_format, value))

    @staticmethod
    def decode(value):
        """Convert isoformat string value to datetime."""
        return datetime.datetime.fromisoformat(value)


class EntityClassDecoder:
    """Decode string to Class object."""

    @staticmethod
    def decode(entity_name):
        """Decode string module:class_name to Class object."""
        return _get_class(entity_name)


def _get_class(topic):
    module_name, name = topic.split(":")
    module = importlib.import_module(module_name)
    return getattr(module, name)


class ValueEncoder:
    """Converting a value object to a jsonable dict."""

    @classmethod
    def encode(cls, value_obj):
        """Encode value object to jsonable dict."""
        encoded = InvariantsEncoder.encode(value_obj.invariants)

        Value = value_obj.__class__
        encoded["__value__"] = "{}:{}".format(Value.__module__, Value.__name__)
        return encoded

    @staticmethod
    def is_value(value_obj):
        """Confirm if it is a value object."""
        try:
            value_obj.foo = 1
            return False
        except AttributeError:
            pass

        return hasattr(value_obj, "invariants")


class ValueClassNotFound(Exception):
    """Raised when importing Value class not found."""

    def __init__(self, module, value_name):
        """Show domain exception."""
        super().__init__(f"Could not find {value_name} in module {module}")


class ValueDictDecoder:
    """Converting value dict to value object."""

    @classmethod
    def decode(cls, value_dict):
        """Decode a value dict compatible to a value object."""
        try:
            value = value_dict.pop("__value__")
            module, value_name = value.split(":")
            Value = _get_class(value)
        except ImportError:
            raise ValueClassNotFound(module, value_name)
        except AttributeError:
            raise ValueClassNotFound(module, value_name)

        invariants = InvariantsDecoder.decode(value_dict)
        return Value(**invariants)

    @staticmethod
    def is_value_dict(value_dict):
        """Confirm if dict is a value object compatible."""
        return (
            type(value_dict) is dict
            and "__value__" in value_dict
            and ":" in value_dict["__value__"]
        )


class InvariantsEncoder:
    """Converting invariant pars of a value object to jsonable dictionary."""

    @classmethod
    def encode(cls, dict_value):
        """Encode invariants to a jsonable dictionary."""
        encoded = {}
        for key, value in dict_value.items():
            try:
                encoded[key] = cls._encode_value(value)
            except TypeError as e:
                raise e
        return encoded

    @classmethod
    def _encode_value(cls, value):
        if UUIDEncoder.is_uuid(value):
            return UUIDEncoder.encode(value)
        elif type(value) is datetime.datetime:
            return value.isoformat()
        elif type(value) is int or type(value) is float or type(value) is str:
            return value
        elif ValueEncoder.is_value(value):
            return ValueEncoder.encode(value)
        elif type(value) is tuple or type(value) is list:
            processed = []
            for v in value:
                processed.append(cls._encode_value(v))
            return tuple(processed)
        elif type(value) is dict:
            return {key: v for key, v in value.items()}
        elif value is None:
            return None
        elif type(value) is bool:
            return value
        else:
            raise TypeError(
                f"Value {value} is {type(value).__name__} " "type, not a invariant"
            )


class InvariantsDecoder:
    """Converting jsonable dict to value object invariants."""

    @classmethod
    def decode(cls, dict_value):
        """Decode a jsonable dict to a dict of invariants."""
        decoded = {}
        for key, value in dict_value.items():
            try:
                decoded[key] = cls._decode_value(value)
            except TypeError as e:
                raise e
        return decoded

    @classmethod
    def _decode_value(cls, value):
        if UUIDHexDecoder.is_hex_uuid(value):
            return UUIDHexDecoder.decode(value)
        elif ValueDictDecoder.is_value_dict(value):
            return ValueDictDecoder.decode(value)
        elif DatetimeStringDecoder.is_isoformat(value):
            return DatetimeStringDecoder.decode(value)
        elif type(value) is tuple or type(value) is list:
            res = []
            for v in value:
                res.append(cls._decode_value(v))
            return tuple(res)
        elif type(value) is int or type(value) is float or type(value) is str:
            return value
        elif type(value) is dict:
            return {key: cls._decode_value(v) for key, v in value.items()}
        elif value is None:
            return
        elif type(value) is bool:
            return value
        else:
            raise TypeError(
                f"Value {value} is a {type(value).__name__}" " type, not a invariant"
            )


class UUIDEncoder:
    """Namespace for encoding uuid to hex string values."""

    @staticmethod
    def encode(value):
        """Encode uuid object to its hex representation."""
        return f"HEX:{value.hex}"

    @staticmethod
    def is_uuid(value):
        """Confirm if value is an UUID object."""
        return type(value) is uuid.UUID


class UUIDHexDecoder:
    """Namespace for decoding hex string uuid."""

    @staticmethod
    def decode(str_value):
        """Decode a hex string value to an uuid."""
        return uuid.UUID(hex=str_value[4:])

    @staticmethod
    def is_hex_uuid(str_value):
        """Confirm if its a uuid representation."""
        return type(str_value) is str and "HEX:" == str_value[:4]

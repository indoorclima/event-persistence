import uuid
import json
import datetime
from psycopg2 import Error, OperationalError, IntegrityError, connect
import psycopg2.extras

from eventence.stores.sql import EventTable, EntityTable, EventView, SqlEventDataAdapter
from eventence.core.commands import (
    EntityAlreadyExist,
    EntityNotFound,
    EntityVersionAlreadyExist,
)
from eventence.ports import EventDataTransfer
from eventence.stores.encoding import InvariantsDecoder, InvariantsEncoder
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
psycopg2.extras.register_uuid()


_table_exist_sql = """
    select exists (
        select * from information_schema.tables
        where table_schema = 'public'
        and table_name = '{name}'); """


class PostgresqlEventDataAdapter(SqlEventDataAdapter):
    """Implementation of EventDataPort using sqlite database."""

    def __init__(self, **connection_kwargs):
        """Implement EventData using sqlite database."""
        self.conn = connect(**connection_kwargs)
        super().__init__(
            self.conn,
            PostgresqlEventTable(),
            PostgresqlEntityTable(),
            PostgresqlEventView(),
        )

    def create_schema(self):
        pass


class PostgresqlEventTable(EventTable):
    """Persist event details in a table."""

    def exist(self, cursor):
        """Exist table%s."""
        cursor.execute(_table_exist_sql.format(name="events"))

        return cursor.fetchone()[0]

    def create(self, cursor):
        """Create entities table into SQlite database."""
        statements = [
            """
            CREATE TABLE events (
            "id" serial primary key,
            "created_on" timestamp,
            "entity_id" uuid,
            "name" varchar(100),
            "version" integer,
            "pars" text,
            "inserted_on" timestamp default now(),
            foreign key (entity_id) references entities (id));
            """,
            """
            create unique index "events_select" on events ("entity_id", "version");
            """,
        ]
        for statement in statements:
            cursor.execute(statement)

    def insert_event(self, cursor, event_dto):
        """Insert into events table event information."""
        try:
            version = event_dto.version
            cursor.execute(
                """insert into events (created_on, entity_id, name, version, pars)
                values (%s, %s, %s, %s, %s) """,
                (
                    event_dto.created_on,
                    event_dto.entity_uuid,
                    event_dto.event_name,
                    version,
                    json.dumps(InvariantsEncoder.encode(event_dto.event_pars)),
                ),
            )
        except IntegrityError:
            raise EntityVersionAlreadyExist(event_dto.entity_uuid, event_dto.version)

        return cursor.lastrowid

    def select_event(self, cursor, event_id):
        """Select events by event_id."""
        cursor.execute("select * from events where id=%s", (event_id,))
        row = cursor.fetchone()
        if row:
            return {
                "id": row[0],
                "created_on": row[1],
                "entity_uuid": row[2],
                "event_name": row[3],
                "version": row[4],
                "event_pars": json.loads(row[5]),
            }


class PostgresqlEntityTable(EntityTable):
    """Entity is written on sql table."""

    def exist(self, cursor):
        """Exist table in database%s."""
        cursor.execute(_table_exist_sql.format(name="entities"))

        return cursor.fetchone()[0]

    def create(self, cursor):
        """Create entities table into SQlite database."""
        statements = [
            """CREATE TABLE entities (
            "id" uuid,
            "module" text,
            "name" varchar(100),
            constraint aggregates_pkey PRIMARY KEY (id)
            );
            """,
            """
            create index entities_idx on entities (module, name);
            """,
        ]
        for statement in statements:
            cursor.execute(statement)

    def insert_entity(self, cursor, event_dto):
        """Insert entity into database."""
        module, name = event_dto.entity_name.split(":")
        try:
            cursor.execute(
                """
            insert into entities (id, module, name)
            values (%s, %s, %s)""",
                (event_dto.entity_uuid, module, name),
            )
        except IntegrityError:
            raise EntityAlreadyExist(event_dto.entity_uuid)

    def select_entity(self, cursor, entity_uuid):
        """Return entity_dto from entity_id."""
        cursor.execute("select * from entities where id= %s", (entity_uuid,))
        row = cursor.fetchone()

        if row:
            return {"entity_uuid": row[0], "entity_name": f"{row[1]}:{row[2]}"}


class PostgresqlEventView(EventView):
    """Read event_dtos from sqlite database."""

    def exist(self, cursor):
        """Exist the view%s."""
        cursor.execute(_table_exist_sql.format(name="entity_events"))

        return cursor.fetchone()[0]

    def create(self, cursor):
        """Create sql view."""
        cursor.execute(
            """
            create view entity_events
            as
            select
            events.id as id, events.name as name, version, created_on,
            entities.id as entity_id, entities.name as entity_name,
            entities.module as entity_module, pars
            from
            events inner join entities on events.entity_id = entities.id;
            """
        )

    def select_event_dtos_by_entity_uuid(self, cursor, entity_uuid):
        """Select all event_dtos of an entity."""
        cursor.execute(
            """select * from entity_events where entity_id = %s
                       order by id""",
            (entity_uuid,),
        )
        rows = cursor.fetchall()
        if not rows:
            raise EntityNotFound(entity_uuid)
        return self._decode_rows(rows)

    def select_all_event_dtos(self, cursor, since_id=0, where_event_filters=""):
        """Select all event_dtos since event id with event type filters."""
        statement = (
            "select * from entity_events where id > %s "
            f"{where_event_filters} order by id"
        )
        cursor.execute(statement, (since_id,))
        rows = cursor.fetchall()
        if not rows:
            return []
        return self._decode_rows(rows)

    def _decode_rows(self, rows):
        event_dtos = []
        for row in rows:
            event_dtos.append(
                EventDataTransfer(
                    id=row[0],
                    event_name=row[1],
                    version=row[2],
                    created_on=row[3],
                    entity_uuid=row[4],
                    entity_name=f"{row[6]}:{row[5]}",
                    event_pars=InvariantsDecoder.decode(json.loads(row[7])),
                )
            )

        return event_dtos

import importlib
from dataclasses import dataclass

from . import projection as prj
from .core import commands
from .stores.sql import SqlEventDataAdapter
from .stores.onmem import OnMemEventDataAdapter


@dataclass
class DbEngine:
    name: str
    parameters: dict | None = None


class Persistence:
    """Factory of elements for persistence layer."""

    def __init__(self, engine: DbEngine):
        self._event_data = self._create_event_data(engine)
        self._projector = prj.Projector(self._event_data)

    def create_store(self):
        """Create Store object for persisted entities manipulation."""
        return commands.Store(self._event_data)

    def register_projection(self, projection: prj.Projection):
        """Register projection for syncronizing with events."""
        self._projector.register_projection(projection)

    def _create_event_data(self, engine: DbEngine):
        if engine.name == "onmem":
            return OnMemEventDataAdapter()

        if engine.name == "sqlite":
            from .stores import sqlite

            module = importlib.import_module("sqlite3")
            Connection = getattr(module, "connect")
            conn = Connection(engine.parameters.pop("filename"), **engine.parameters)
            return SqlEventDataAdapter(
                conn,
                sqlite.SqliteEventTable(),
                sqlite.SqliteEntityTable(),
                sqlite.SqliteEventView(),
            )

        if engine.name == "postgresql":
            from .stores import postgresql

            module = importlib.import_module("psycopg2")
            Connection = getattr(module, "connect")
            conn = Connection(**engine.parameters)

            return SqlEventDataAdapter(
                conn,
                postgresql.PostgresqlEventTable(),
                postgresql.PostgresqlEntityTable(),
                postgresql.PostgresqlEventView(),
            )

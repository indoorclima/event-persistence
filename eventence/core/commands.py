"""Implement store for entities.

As a developer, in order to simplify persistence of entities,
I want to have a unique store to save and load entities.
"""
import datetime
from eventence.ports import EventDataTransfer
from threading import Lock


class EntityAlreadyExist(Exception):
    """Exceptin when entity has already persisted on stor."""

    def __init__(self, entity_uuid):
        """Raise when trying to persist previous persisted entity."""
        super().__init__(f"Entity with uuid {entity_uuid} already persisted.")


class EntityNotFound(Exception):
    """Exception when entity is not found in store."""

    def __init__(self, entity_uuid):
        """Raise when not found in store."""
        super().__init__(f"Entity with uuid {entity_uuid} has not been found.")


class EntityVersionAlreadyExist(Exception):
    """Exception when version of entity was already stored."""

    def __init__(self, entity_uuid, version):
        """Raise when version was already stored."""
        super().__init__(
            f"Version of entity with uuid {entity_uuid}" " has been already persisted"
        )


class EventHandler:
    """Helper for comunicating events outside entities."""

    def __init__(self, entity):
        """Handle events triggered by entities."""
        self._entity = entity
        self._history = []
        self._observers = []
        self._callbacks = []
        self._version = -1

    def add_observer(self, observer):
        """Add observer to notify event_triggered."""
        self._observers.append(observer)
        for event_dto in self._history:
            observer.event_is_triggered(event_dto)

    def add_callback(self, callback):
        self._callbacks.append(callback)
        for event_dto in self._history:
            callback(event_dto)

    def _notify(self, event_dto):
        """Notify to all observers an event is trigered."""
        for observer in self._observers:
            observer.event_is_triggered(event_dto)
        for callback in self._callbacks:
            callback(event_dto)

    def add_to_history(self, event):
        """Include into history the event."""
        self._version += 1
        dto = EventDataTransfer.create_from_domain(self._entity, self.version, event)

        self._history.append(dto)
        self._notify(dto)

    def build_entity(self, event_dtos):
        """Build entity from a list of event dtos."""
        for dto in event_dtos:
            self._version = dto.version
            event = dto.build_event()
            event.apply_on(self._entity)

    def clean_history(self):
        """Clean history of entity."""
        del self._history
        self._history = []

    @property
    def entity(self):
        """Return entity linked to event handler."""
        return self._entity

    @property
    def history(self):
        """Return history of entity."""
        return [dto.copy() for dto in self._history]

    @property
    def version(self):
        """Return current version of entity."""
        return None if self._version == -1 else self._version


def _inject_event_builder(init):
    def func(self, *args, **kwargs):
        init(self, *args, **kwargs)
        self.event_handler = EventHandler(self)

    return func


def _catch_event(trigger_event):
    def add_to_history(self, event):
        trigger_event(self, event)
        self.event_handler.add_to_history(event)

    return add_to_history


class EventRegister:
    """Singleton class for registering entities to be event sourced."""

    _registered = {}

    @classmethod
    def register_class(cls, Entity):
        """Inject into Entity class a event_handler as a virus!."""
        if Entity in cls._registered:
            return

        init = getattr(Entity, "__init__")
        setattr(Entity, "__init__", _inject_event_builder(init))

        trigger_event = getattr(Entity, "trigger_event")
        setattr(Entity, "trigger_event", _catch_event(trigger_event))

        cls._registered[Entity] = (init, trigger_event)

    @classmethod
    def unregister_class(cls, Entity):
        """Leave Entity class as initially is, without event handler."""
        init, trigger_event = cls._registered.pop(Entity)
        setattr(Entity, "__init__", init)
        setattr(Entity, "trigger_event", trigger_event)

    @classmethod
    def unregister_all(cls):
        """Unregister all classes of application."""
        for Entity in list(cls._registered.keys()):
            cls.unregister_class(Entity)

    @classmethod
    def is_registered(cls, Entity):
        """Confirm if a class Entity is registered."""
        return Entity in cls._registered


class Store:
    """Class to make persistence of entities."""

    def __init__(self, event_data_port):
        """Persist entities of domain."""
        self.data = event_data_port
        self._cached_entities = []
        self._lock = Lock()

    def load(self, entity_id, version_date=None):
        """Retrieve entity from store."""
        if version_date is None:
            version_date = datetime.datetime.now()
        try:
            event_dtos = self.data.get_entity_event_dtos(entity_id.uuid)
        except EntityNotFound:
            return

        event_dtos = [dto for dto in event_dtos if dto.created_on < version_date]

        Entity = event_dtos[0].entity_class
        entity = Entity()
        if not hasattr(entity, "event_handler"):
            EventRegister.register_class(Entity)
            entity = Entity()

        entity.event_handler.build_entity(event_dtos)
        self._cached_entities.append(entity)

        return entity

    def save(self, *entities):
        """Save or update an entity to store."""
        with self._lock:
            if not entities:
                return self.save(*self._cached_entities)

            event_dtos = []
            for entity in entities:
                if not hasattr(entity, "event_handler"):
                    raise Exception("Entity does not have event_handler")

                if entity not in self._cached_entities:
                    self._cached_entities.append(entity)

                event_dtos.extend(entity.event_handler.history)

            if event_dtos:
                self.data.add_event_dtos(event_dtos)

            for entity in entities:
                entity.event_handler.clean_history()

    def __delete__(self):
        """Restore recorded classes to initial state."""
        EventRegister.unregister_all()

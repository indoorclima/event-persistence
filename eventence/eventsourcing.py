import importlib
import logging
import threading

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class AggregateAlreadyExist(Exception):
    pass


class AggregateVersionConflict(Exception):
    pass


class AggregateNotFound(Exception):
    pass


class AbsentSubscriber(Exception):
    pass


class SubscriberNotFound(Exception):
    pass


class EventBroker:
    def __init__(self, broker, event_data):
        self._broker = broker
        self._data = event_data
        self._starts = {}
        self._lock = threading.Lock()

    def publish(self, event_dtos):
        with self._lock:
            for event_dto in event_dtos:
                aggregate_class = event_dto["aggregate"]["class"]
                topic = "{}:{}".format(
                    aggregate_class.__module__, aggregate_class.__name__
                )

                self._broker.publish(event_dto, topic)

    def subscribe(self, subscriber, Aggregate=None, last_event_id=None):
        if subscriber not in self._starts:
            self._starts[subscriber] = []

        self._starts[subscriber].append((Aggregate, last_event_id))

    def start_subscription(self, subscriber):
        with self._lock:
            subscriptions = self._starts.pop(subscriber, None)
            if not subscriptions:
                raise SubscriberNotFound()

            all_events = []

            for Aggregate, last_event_id in subscriptions:
                all_events.extend(self._data.collect_events(Aggregate, last_event_id))
            all_events = sorted(all_events, key=lambda event: event["id"])
            for event_dto in all_events:
                subscriber.event_raised(event_dto)

            for Aggregate, _ in subscriptions:
                self._broker.subscribe(subscriber.event_raised, Aggregate.topic)

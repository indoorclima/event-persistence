"""Interfacing module to customize library."""

import abc
import datetime
import uuid
from dataclasses import dataclass, field
from eventence.stores.encoding import (
    InvariantsDecoder,
    InvariantsEncoder,
    EntityClassDecoder,
)


class EventDataPort(abc.ABC):
    """Interface to implement EventDataStores."""

    @abc.abstractmethod
    def add_event_dtos(self, event_dtos):
        """Insert a list of event data transfers."""
        ...

    @abc.abstractmethod
    def get_entity_event_dtos(self, entity_id):
        """Retrieve a list of all event dtos of an entity."""
        ...

    @abc.abstractmethod
    def collect_event_dtos(self, from_id, event_types):
        """Retrieve a list of events filtered by event type > event_id fs."""
        ...


class ProjectionDataPort(abc.ABC):
    """Interface to implement ProjectionData."""

    @abc.abstractproperty
    def projection_name(self):
        """Name of projection."""
        ...

    @abc.abstractproperty
    def last_event_index(self):
        """Last event id processed by ProjectionData."""
        pass

    @abc.abstractmethod
    def update(self, event_dto):
        """Update data with the event_dto received."""
        ...

    @abc.abstractproperty
    def events_filter(self):
        """List of event types to notify to object."""
        ...


class EventObserver(abc.ABC):
    """Interface for a tipical event observer."""

    @abc.abstractmethod
    def event_is_triggered(self, event_dto):
        """Recieve event_dto from an entity which has triggered event."""
        ...


@dataclass
class EventDataTransfer:
    """
    Data class containing event data transfer objects.

    Data class for persistence of events to data and retrieving them.
    Contain all useful methods for conversions.
    """

    entity_name: str
    entity_uuid: uuid.UUID
    version: int | None
    event_name: str
    event_pars: dict
    created_on: datetime.datetime = field(default_factory=datetime.datetime.now)
    id: int = None

    def build_event(self):
        """Build event object from event_dto."""
        Event = getattr(self.entity_class, self.event_name)
        invariants = self.event_pars
        return Event(**invariants)

    @property
    def entity_class(self):
        """Return Entity class."""
        return EntityClassDecoder.decode(self.entity_name)

    @classmethod
    def create_from_domain(cls, entity, version, event):
        """Create event_dto from domain objects."""
        return cls(
            f"{entity.__class__.__module__}:{entity.__class__.__name__}",
            entity.id.uuid,
            version,
            event.__class__.__name__,
            event.invariants,
        )

    def copy(self):
        """Duplicate dto."""
        return EventDataTransfer(
            self.entity_name,
            self.entity_uuid,
            self.version,
            self.event_name,
            self.event_pars,
            self.created_on,
            self.id,
        )

    def __eq__(self, o):
        """Compare all fields shall be equal."""
        return (
            o.__class__ == self.__class__
            and self.entity_name == o.entity_name
            and self.version == o.version
            and self.entity_uuid == o.entity_uuid
            and self.event_name == o.event_name
            and self.event_pars == o.event_pars
            and self.created_on == o.created_on
            and self.id == o.id
        )

        return eq

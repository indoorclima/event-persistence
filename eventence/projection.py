"""Implement queries on eventsourcing using projector pattern."""
import abc
from dataclasses import dataclass
from typing import Callable


@dataclass
class EventHook:
    entity_name: str
    event_name: str | None
    callback: Callable


class Projection(abc.ABC):
    @abc.abstractproperty
    def event_hooks(self) -> list[EventHook]:
        ...

    @abc.abstractproperty
    def last_event_id(str) -> int:
        ...


class Projector:
    """Collect all queries modelled as projections of event_data."""

    def __init__(self, event_data):
        """Collect queries given a event_data."""
        self._event_data = event_data
        self._projections = []

    def register_projection(self, projection):
        self._projections.append(projection)
        self._syncronize_with_event_data()

    def _syncronize_projection(self, projection):
        event_filters = [
            f"{hook.entity_name}.{hook.event_name}" for hook in projection.event_hooks
        ]
        event_dtos = self._event_data.collect_event_dtos(
            projection.last_event_id, event_filters
        )

        for event_dto in event_dtos:
            for hook in projection.event_hooks:
                entity_name = event_dto.entity_name.split(":")[1]
                if (
                    hook.entity_name == entity_name
                    and hook.event_name == event_dto.event_name
                ):
                    hook.callback(event_dto)

    def _syncronize_with_event_data(self):
        """Syncronize event_data with projections datas."""
        for projection in self._projections:
            self._syncronize_projection(projection)

    def syncronize_with_event_broker(self, event_broker):
        ...
